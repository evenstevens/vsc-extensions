First, add 'code' to your command line. To do this, open Visual Studio Code, bring up the command palette (Cmd + Shift + P) and type shell command to find the Shell Command: Install 'code' command in PATH command.
To generate your list of extensions run the following command in the terminal:
code --list-extensions
This list can be installed using the following command if you have xargs, assuming the terminal is in the repo's root directory:
< ext.txt tr '\n' '\0' | xargs -0 -I{} code --install-extension {}